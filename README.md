Results from Alasdair Tran for active learning

# Getting started

## Install dependencies

You will need to install or have installed
* [git lfs](https://git-lfs.github.com/) (on linux, run the PackageCloud bash script, then `sudo apt-get install git-lfs`)
* python3, ipython and sklearn (recommend installing [anaconda](https://www.continuum.io/downloads) if this is a fresh setup, it will install python, ipython and jupyter). Additionaly install sklearn using
```
conda install scikit-learn
```

## Set up git lfs

Make sure you install git lfs and set it up the way you want before cloning the repository.

Do ONCE
```
git lfs install
```
for git-lfs-1.1.0 and above (or `git lfs init` for older versions).

Normally, lfs tracked files will download automatically each time you clone, pull or fetch.  Optionally, to avoid this and always manually download lfs files, set globally:
```
git config --global filter.lfs.smudge "git-lfs smudge --skip %f"
```
Then, to get large files after cloning or after a new one has been added
```
git lfs pull
```
To get a single file
```
git lfs fetch -I [path_from_repo_root]
git lfs checkout [path_from_repo_root]
```

Since git lfs uses https it asks for your password a lot.  You can set git up to cache your password using
```
git config --global credential.helper cache
```
More info on password caching is [here](https://help.github.com/articles/caching-your-github-password-in-git/).

## Clone repo and grab the data

Clone the repository using https (git lfs doesn't work with ssh) and download the large files if you added the smudge settings.

    ```
    git clone https://gitlab.com/chengsoon.ong/motime.git
    cd motime
    git lfs pull
    ```
If you cloned before installing git-lfs, the data files will be text pointers instead of the real files, [this](https://github.com/github/git-lfs/issues/325) might help you to fix it.

# Quick guide to git lfs

To track an individual file with git lfs
```
git lfs track [path_from_repo_root]
```
To always track all files with a certain extention (note the quotes, if you omit them then you will only track those files in your directory that match *.extension right now, rather than those added later)
```
git lfs track "*.extension"
```
Then use git add and commit the file as per normal.

Read [gitlab's documentation](http://doc.gitlab.com/ce/workflow/lfs/manage_large_binaries_with_git_lfs.html)
for a longer overview of how git large file storage works.

# Quick guide to the Results

They are stored as binary npy files. They can be read by, for example, calling
```
sklearn.externals.joblib.load('results/sdss/margin/results.pkl')
```

with scikit-learn. This will give you a dictionary with 4 keys:

* accuracy: an array of shape (10, n) containing the accuracy rates, where 10 is the number of folds and n is the number of active learning queries.
* mpba: mean posterior balanced accuracy, same shape as above
* f1: f1-score, same shape as above
* time: time taken to run (in seconds)

More details are in [this notebook](https://github.com/chengsoonong/mclass-sky/blob/master/projects/peerjcs16/active_learning_suggestions.ipynb).
